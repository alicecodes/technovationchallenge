package com.cs.westviewhs.technovationchallenge;

import android.app.ListActivity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

import java.util.ArrayList;


public class MyContacts extends ListActivity {
    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_contacts);
        // adapter=new ArrayAdapter<String>(this,
           //     android.R.layout.simple_list_item_1,
             //   listItems);
        // setListAdapter(adapter);
    }

    public void addItems(View v) {
        startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI),1001);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        return;
    }

}
