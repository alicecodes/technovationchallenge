package com.cs.westviewhs.technovationchallenge;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Tutorial extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        Button tut1 = (Button) findViewById(R.id.button);
        tut1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(getBaseContext(), Level1Tutorial.class));
            }
        });

        Button tut2 = (Button) findViewById(R.id.button8);
        tut2.setOnClickListener(new View.OnClickListener()

        {
            public void onClick (View v){
                startActivity(new Intent(getBaseContext(), Level2Tutorial.class));
            }
        });

        Button tut3 = (Button) findViewById(R.id.button9);
        tut3.setOnClickListener(new View.OnClickListener()

        {
            public void onClick (View v){
                startActivity(new Intent(getBaseContext(), Level3Tutorial.class));
            }
        });
    }
}
